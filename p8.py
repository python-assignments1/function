class building:
    #class variable
    buildingName="Sinhgad Vridavan"
    
    #Constructor
    def __init__(self,roomNo,rent):
        print("In Constructor")
        #instance  variable
        self.roomNo=roomNo
        self.rent=rent
    
    #instanceMethod
    def roomInfo(self):
        print(self.roomNo)
        print(self.rent)
        print(self.buildingName)

obj1=building(403,"Rent clear")
obj2=building(401,"Rent remain")

obj1.roomInfo()
obj2.roomInfo()
