class parentfun():

    def digitCount(self,l):
        ans=[]
        for i in range(len(l)):
            temp=l[i]
            count=0
            while(temp>0):
                count+=1
                temp//=10
            ans.append(count)
        return ans

    def evenDigit(self,l):
        ans=[]
        for i in range(len(l)):
            temp=l[i]
            count=0
            while(temp>0):
                rem=temp%10
                if rem%2==0:
                    count+=1
                temp//=10
            ans.append(count)
        return ans

    def oddDigit(self,l):
         ans=[]
         for i in range(len(l)):
             temp=l[i]
             count=0
             while(temp>0):
                 rem=temp%10
                 if rem%2==1:
                     count+=1
                 temp//=10
             ans.append(count)
         return ans

n=int(input("Enter the size of list: "))
print("Enter the list element: ")
l=[]
for i in range(0,n):
    l.append(int(input(f"Enter element no. {i+1} : ")))

print(l)
print("choose the option what to do : 1.DigitCount 2.EvenDigitCount 3.OddDigitCount")
op=int(input("Enter option number: "))

obj=parentfun()
if op==1:
    result=obj.digitCount(l)
    print(result)
elif op==2:
    result=obj.evenDigit(l)
    print(result)
elif op==3:
    result=obj.oddDigit(l)
    print(result)
else:
    print("Enter valid option.. :)")
