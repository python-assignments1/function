import array as arr

def fact(n):
    a=arr.array('i',[])
    f=1
    temp=0
    for j in range(0,len(n)):
        temp=n[j]
        for i in range(temp,0,-1):
            f*=i
        a.append(f)
        f=1
    return a

n=arr.array('i',[3,4,7,5])
ans=fact(n)
print(ans)
