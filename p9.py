
class IPL:
    #class variable
    liveOn="JioCinema"

    #Constructor
    def __init__(self,teamName,captain):
        print("In Constructor")
        #instance variable
        self.teamName=teamName
        self.captain=captain

    #instance method
    def teamInfo(self):
        print(self.teamName)
        print(self.captain)
        print(IPL.liveOn)

obj1=IPL("CSK","Mahendra Singh Dhoni")
obj2=IPL("MI","Rohit Shrama")

obj1.teamInfo()
obj2.teamInfo()
